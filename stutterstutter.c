// ================================================================================
// name: stutterstutter~.c
// desc: extern for pd that creates a stutter/beat repeat effect
//      * takes in:
//          - a signal to be modified, bangs to turn stutterning on/off
//          - stutter length (ms)
//          - stutter break length (ms)
//          - wet/dry level of stutters (0 to 100%)
//          - randomness of stutters occuring when stutters are on (0 to 100%)
//          - amplitude LFO to modify the amplitude of the stutters (signal)
//          - set messages to control:
//              envelope on/off and length of envelope
//              stutter/mute setting on/off
//              transient on/off for input with more transients
//      * outputs
//          - modified signal
//          - bangs if transient detector is on
//
// author: jennifer hsu
// date: fall 2013
// =================================================================================

#include "m_pd.h"
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

// stutters/stutter break states
#define STUTTER 0
#define STUTTER_BREAK 1
// transient detection states
#define WAIT_FOR_PEAK 2
#define WAIT_FOR_DIP 3
// internal states to keep track of mute or stutter setting
#define SETTING_MUTE 0
#define SETTING_STUTTER 1
// transient detection on or off
#define TRACKER_OFF 0
#define TRACKER_ON 1
// envelope state
#define ENV_OFF 0
#define ENV_ON 1
#define ENV_A 2 // ramp from 0 to 1
#define ENV_S 3 // sustain at 1
#define ENV_D 4 // ramp from 1 to 0
// amplitude state
#define AMP_LFO_ON 0
#define AMP_LFO_OFF 1


#define MAX_DELAY_SIZE 16777216 // value from Tom's delay line
#define MAX_ENV_SIZE 220500 // 5 second long envelope


// struct type to hold all variables that have to do with the stutterstutter~ object
typedef struct _stutterstutter
{
    t_object x_obj;
    
    // ==== delay line stuff ====
    t_float sr;                     // sample rate
    t_float *delayBuffer;           // buffer to hold delayed samples
    t_float delayTime;              // delay in ms
    t_float delaySamples;           // delay in samples
    //t_float delaySamplesCurrent;    // current delay in samples (more like previous)
    t_int delaySize;                // max size of entire delay line in samples
    t_int writePos;                 // delay buffer write position
    // note: the writePos moves forward in the delay line to make reading
    //  for the stutters conceptually easier for me
    //  delay line is does not account for fractional delays (not needed for this application)
    // ==========================
    
    // === stutter chunk stuff ===
    t_float stutterTimeTarget, stutterTimeCurrent;  // stutter chunk time in ms
    t_int stutterSamps;                             // stutter chunk in samples
    t_int readPos;                                  // stutter chunk reader position in delay line
    t_int stutterStartSamp;                         // stutter chunk start sample
    t_int stutterEndSamp;                           // stutter chunk end sample
    t_int stutterOn;                                // boolean for whether the stuttering should be played or not
    t_int stutterSampsRead;                         // boolean that tells how many stutter samples have been read
                                                    //  to easily wrap our reader
    // ===========================
    
    // === stutter chunk break stuff ===
    t_float stutterbTimeTarget, stutterbTimeCurrent;// stutter break time in ms
    t_float stutterbSamps;                          // stutter break chunk time in samples
    t_int stutterbCnt;                              // stutter break sample counter
    t_int stutterOrBreak;                           // boolean to see whether we are on a stutter or a break
    // =================================
    
    // ==== stutter parameters ====
    t_float wetdryTarget, wetdryCurrent;
    t_float randomnessTarget, randomnessCurrent;
    t_int randomNoStutter;           // if 1 -> don't play this stutter, 0 --> play this stutter
    t_float ampTarget, ampCurrent;
    t_int ampState;                  // LFO or control rate
    t_int muteOrStutter;             // 0 --> mute the incoming signal for stutter time,
                                     // 1 --> play stutters from delay line
    // ============================
    
    
    // === level tracker stuff ===
    t_float levelEstimate;          // current level estimate of level detector
    t_float thresholdU;             // upper threshold
    t_float thresholdL;             // lower threshold
    t_outlet * bangOutlet;          // bang outlet so we know when a peak was detected
    t_int lastPeakSamp;             // keeps track of the last detected transient
    t_int peakDetectorState;        // keeps track of current state of the level tracker
    t_int levelTrackerState;        // 0 --> level tracker is off
                                    // 1 --> level tracker is on
    t_int detectedPeak;             // 0 to start with, 1 if we ever detected a peak
    // note: the level tracker is currently hardcoded
    // this means that the user cannot control the attack/decay and upper/lower thresholds
    // ===========================
    
    
    // === envelope parameters ===
    t_float * env;                  // envelope samples
    t_float envSampsTarget, envSampsCurrent; // number of envelope samples that user wants
    t_int envState;                 // envelope on or off
    t_int envCntr;                  // counter to access envelope samples
    t_int envASDstate;               // envelope attack or decay state
    // ===========================
    
} t_stutterstutter;

// stutterstutter~ class that will be created in 'setup'
// and used in 'new' to create new instances
t_class *stutterstutter_class;

/* function prototypes */
static t_int *stutterstutter_perform(t_int *w);
static void stutterstutter_dsp(t_stutterstutter *x, t_signal **sp);
void stutterstutter_wetdry(t_stutterstutter *x, t_floatarg f);
void stutterstutter_randomness(t_stutterstutter *x, t_floatarg f);
void stutterstutter_amplitude(t_stutterstutter *x, t_floatarg f);
void messageUsage(void);
void stutterstutter_set(t_stutterstutter *x, t_symbol *selector, int argc, t_atom *argv);
static void stutterstutter_free(t_stutterstutter *x);
static void *stutterstutter_new(void);
void stutterstutter_tilde_setup(void);


// perform function (audio callback)
static t_int *stutterstutter_perform(t_int *w)
{
    
    // extract data
    t_stutterstutter *x = (t_stutterstutter *)(w[1]);
    t_float *in = (t_float *)(w[2]);
    t_float *ampLFO = (t_float *)(w[3]);
    t_float *out = (t_float *)(w[4]);
    int blockSize = (int)(w[5]);
    
    
    // hardcode level tracker data for now
    float b0_a = 1.0f - exp(-1.0f / 0.5f * x->sr);
    float b0_r = 1.0f - exp(-1.0 / 0.02 * x->sr);
    
    // holder
    float dBLevel;
    
    
    // ============================================
    // delay stuff
    // ============================================
    //long readPos, readPosP1;
    //t_float readPosFloat, readPosFrac;
    
    t_float inSamp, outSamp;
    t_float ampInc, wetdryInc, randomnessInc;
    t_float stutterTimeInc, stutterbTimeInc;
    t_float envInc;
    t_float ampSamp;
    
    t_float oneoverblocksize = 1.0f/blockSize;
    t_float oneover1000 = 1.0f/1000.0f;
    
    // get delay time in samples
    x->delaySamples = x->delayTime * oneover1000 * x->sr;
    
    // calculate parameter increments
    ampInc = (x->ampTarget - x->ampCurrent) * oneoverblocksize;
    wetdryInc = (x->wetdryTarget - x->wetdryCurrent) * oneoverblocksize;
    randomnessInc = (x->randomnessTarget - x->randomnessCurrent) * oneoverblocksize;
    stutterTimeInc = (x->stutterTimeTarget - x->stutterTimeCurrent) * oneoverblocksize;
    stutterbTimeInc = (x->stutterbTimeTarget - x->stutterbTimeCurrent) * oneoverblocksize;
    envInc = ( float )(x->envSampsTarget - x->envSampsCurrent) * oneoverblocksize;
    t_float diff = (x->envSampsTarget - x->envSampsCurrent);
    diff = diff + 0;
    
    
    // process samples
    int sample;
    for(sample = 0; sample < blockSize; sample++)
    {
        
        // ============================================
        // delay line
        // ============================================
        
        
        // dezippering
        x->ampCurrent += ampInc;
        x->wetdryCurrent += wetdryInc;
        x->randomnessCurrent += randomnessInc;
        x->stutterTimeCurrent += stutterTimeInc;
        x->stutterbTimeCurrent += stutterbTimeInc;
        x->envSampsCurrent += envInc;
        
        // copy input sample
        inSamp = *(in+sample);
        
        // put sample into delay line
        *(x->delayBuffer + x->writePos) = inSamp;
        
        // modify amplitude based on amplitude state
        if(x->ampState == AMP_LFO_ON)
        {
            ampSamp = *(ampLFO+sample);
        } else
        {
            ampSamp = x->ampCurrent;
        }
        
        // update envelope states
        if(x->envState == ENV_ON)
        {
            if(x->stutterSampsRead >= 0 && x->stutterSampsRead < x->envSampsCurrent)
            {
                x->envASDstate = ENV_A;
            } else if(x->stutterSampsRead >= x->stutterSamps - x->envSampsCurrent && x->stutterSampsRead <= x->stutterSamps )
            {
                x->envASDstate = ENV_D;
            } else
            {
                x->envASDstate = ENV_S;
            }
        }
        
        // ======== STUTTER READ AND OUTPUT ========
        if(x->stutterOn)
        {
            
            // --- calculate stutter length and set up readers ---
            // reset stutter chunk pointers
            x->stutterSamps = ( int )(x->stutterTimeCurrent / 1000.0f * x->sr);
                
            // set end sample for the chunk (lengthening or shortening the chunk)
            x->stutterEndSamp = x->stutterStartSamp - x->stutterSamps;
                
            // wrap
            while( x->stutterEndSamp < 0 )
                x->stutterEndSamp += x->delaySize;
            while( x->stutterEndSamp >= x->delaySize )
                x->stutterEndSamp -= x->delaySize;
                
            // set read position if it's past the end sample
            if(x->stutterSampsRead >= x->stutterSamps)
            {
                x->readPos = x->stutterStartSamp;
                x->stutterSampsRead = 0;
            }
            
            // ----------------------------------------------------
            
            // -- calculate stutter break time length and set up counters ---
            // get break time in samples
            x->stutterbSamps = ( int )x->stutterbTimeCurrent / 1000.0f * x->sr;
            
            if(!x->stutterOn || x->stutterbCnt >= x->stutterbSamps)
            {
                // set stutter break counter
                x->stutterbCnt = 0;
            }
            // ---------------------------------------------------------------
            
            // -- check that envelope is still correct --
            if(x->envState == ENV_ON)
            {
                // just for envelope to change to half of the stutter samps
                if( x->envSampsTarget > floor(( float )x->stutterSamps/2.0f) )
                {
                    x->envSampsTarget = ( int )floor(x->stutterSamps*0.5f);
                }
            }
            // ---------------------------------------------------------------
            
            // change output depending on stutter or break state
            if(x->stutterOrBreak == STUTTER)
            {
                
                if(x->randomNoStutter == 1)
                {
                    // silence the output from the stutter
                    outSamp = inSamp * x->wetdryCurrent;
                } else
                {
                    // calculate output sample
                    
                    if( x->muteOrStutter == SETTING_MUTE )
                    {
                        
                        // in mute setting (read from input)
                        
                        if( x->envState == ENV_ON && x->envASDstate != ENV_S)
                        {
                            if(x->envASDstate == ENV_A)
                            {
                                // envelope is ramping from 0 to 1
                                if(x->envSampsCurrent > 0)
                                {
                                    outSamp = inSamp * x->wetdryCurrent + inSamp * (1.0f-x->wetdryCurrent) * ( float )(x->envCntr/x->envSampsCurrent) * ampSamp;
                                } else
                                {
                                    // this is called if the envelope samples have not been modified yet
                                    outSamp = inSamp * x->wetdryCurrent + inSamp * (1.0f-x->wetdryCurrent) * ampSamp;
                                }
                                // check if we should change envelope states
                                if(x->envCntr >= x->envSampsCurrent)
                                {
                                    x->envASDstate = ENV_S;
                                } else
                                {
                                    x->envCntr++;
                                }
                            }
                            if(x->envASDstate == ENV_D)
                            {
                                // envelope is ramping from 1 to 0
                                if(x->envSampsCurrent > 0)
                                {
                                    outSamp = inSamp * x->wetdryCurrent + inSamp * (1.0f-x->wetdryCurrent) * ( float )(x->envCntr/x->envSampsCurrent) * ampSamp;
                                } else
                                {
                                    // called if envelope has not yet been specified
                                    outSamp = inSamp * x->wetdryCurrent + inSamp * (1.0f-x->wetdryCurrent) * ampSamp;
                                }
                                // check and update envelope states
                                if(x->envCntr <= 0)
                                {
                                    x->envASDstate = ENV_S;
                                } else
                                {
                                    x->envCntr--;
                                }
                            }
                            
                        } else
                        {
                            outSamp = inSamp * x->wetdryCurrent + inSamp * (1.0f-x->wetdryCurrent) * ampSamp;
                        }
                        
                    } else
                    {
                        // in stutter setting (read from delay line)
                        
                        if( x->envState == ENV_ON && x->envASDstate != ENV_S)
                        {
                            if(x->envASDstate == ENV_A)
                            {
                                // envelope is ramping from 0 to 1
                                if(x->envSampsCurrent > 0)
                                {
                                    outSamp = inSamp * x->wetdryCurrent + *(x->delayBuffer+x->readPos) * (1.0f-x->wetdryCurrent) * ( float )(x->envCntr/x->envSampsCurrent) * ampSamp;
                                } else
                                {
                                    // envelope has not been specified yet
                                    outSamp = inSamp * x->wetdryCurrent + *(x->delayBuffer+x->readPos) * (1.0f-x->wetdryCurrent) * ampSamp;
                                }
                                // check and update envelope states
                                if(x->envCntr >= x->envSampsCurrent)
                                {
                                    x->envASDstate = ENV_S;
                                } else
                                {
                                    x->envCntr++;
                                }
                            }
                            if(x->envASDstate == ENV_D)
                            {
                                // envelope is ramping from 1 to 0
                                if(x->envSampsCurrent > 0)
                                {
                                    outSamp = inSamp * x->wetdryCurrent + *(x->delayBuffer+x->readPos) * (1.0f-x->wetdryCurrent) * ( float )(x->envCntr/x->envSampsCurrent) * ampSamp;
                                } else
                                {
                                    // envelope has not yet been specified
                                    outSamp = inSamp * x->wetdryCurrent + *(x->delayBuffer+x->readPos) * (1.0f-x->wetdryCurrent) * ampSamp;
                                }
                                // check and update envelope states
                                if(x->envCntr <= 0)
                                {
                                    x->envASDstate = ENV_S;
                                } else
                                {
                                    x->envCntr--;
                                }
                            }
                        } else
                        {
                            // read from delay line stutter
                            outSamp = inSamp * x->wetdryCurrent + *(x->delayBuffer+x->readPos) * (1.0f-x->wetdryCurrent) * ampSamp;
                        }
                    }
                }
                
                x->stutterSampsRead++;
                x->readPos--;
                
                // wrap around whole delay line
                while( x->readPos < 0 )
                    x->readPos += x->delaySize;
                while( x->readPos >= x->delaySize )
                    x->readPos -= x->delaySize;
                
                // wrap around stutter chunk
                if(x->stutterSampsRead == x->stutterSamps)
                {
                    x->readPos = x->stutterStartSamp;
                    x->stutterSampsRead = 0;
                    // calculate next stutter based on randomness
                    float choice = ( float )rand( ) / RAND_MAX;
                    if( choice < x->randomnessCurrent )
                    {
                        // play this stutter
                        x->randomNoStutter = 0;
                    } else
                    {
                        // turn this stutter off
                        x->randomNoStutter = 1;
                    }
                    
                    // set state
                    x->stutterOrBreak = STUTTER_BREAK;
                }
            } else
            {
                // stutter break
                
                // calculate output sample
                outSamp = inSamp * x->wetdryCurrent;
                
                // increment break counter
                x->stutterbCnt++;
                // wrap and switch stutter states
                if(x->stutterbCnt >= x->stutterbSamps)
                {
                    x->stutterbCnt = 0;
                    x->stutterOrBreak = STUTTER;
                }
            }
            
            
            
        } else
        {
            // just let out the input sample
            outSamp = inSamp * x->wetdryCurrent;
        }
        // ==========================================
         
        

        
        // ============================================
        // level tracker stuff
        // ============================================
        // we'll keep track of things even if the user specifies it as off
        // just so we keep a running measure of the level
        
        // update level estimate
        float absIn = fabs(*(in+sample));
        if( absIn > x->levelEstimate )
        {
            x->levelEstimate += b0_a * (absIn - x->levelEstimate);
        } else
        {
            x->levelEstimate += b0_r * (absIn - x->levelEstimate);
        }
        
        // get levelEstimate in dB
        if( x->levelEstimate > 0.00001 )
        {
            // avoid taking log of 0
            dBLevel = 20.0f * log10f(x->levelEstimate);
        } else
        {
            // set to -100dB
            dBLevel = -100.0f;
        }
        
        // check levelEstimate with level thresholds
        // change to dB
        if(x->levelEstimate >= x->thresholdU && x->peakDetectorState == WAIT_FOR_PEAK)
        {
            // hit upper threshold, now wait for lower threshold
            x->peakDetectorState = WAIT_FOR_DIP;
        }
        if(x->levelEstimate <= x->thresholdL && x->peakDetectorState == WAIT_FOR_DIP)
        {
            x->detectedPeak = 1;
            // save our peak sample
            x->lastPeakSamp = x->writePos;
            // wrap
            x->lastPeakSamp %= x->delaySize;
            // bang to let user know we tracked a peak
            outlet_bang(x->bangOutlet);
            
            x->peakDetectorState = WAIT_FOR_PEAK;
        }
        
        // ===========================================
        
        // output sample
        *(out+sample) = outSamp;
        
        // decrement writer
        x->writePos--;
        if(x->writePos < 0)
            x->writePos += x->delaySize;
     
    }
    
    return (w+6);
}

// called when dsp starts, register audio callback
static void stutterstutter_dsp(t_stutterstutter *x, t_signal **sp)
{
    // save this here because I can't get it to work in the add function...
    x->sr = sp[0]->s_sr;
    // add the perform function
    dsp_add(stutterstutter_perform, 5, x, sp[0]->s_vec, sp[1]->s_vec, sp[2]->s_vec, sp[0]->s_n);
    
}

// called when we receive a bang - starts and stops stutters
void stutterstutter_bang(t_stutterstutter *x)
{
    if(!x->stutterOn)
    {
        x->stutterOn = 1;
        
        // init chunk read positions
    
        // get time in samples
        x->stutterSamps = ( int )(x->stutterTimeCurrent / 1000.0f * x->sr);
    
        if(x->levelTrackerState == TRACKER_OFF || (x->levelTrackerState == 1 && x->detectedPeak == 0))
        {
            // with the transient detector turned off
            // or transient detector turned on but with no detected peak
        
            // set start and end samples for the chunk
            x->stutterEndSamp = x->writePos;
            x->stutterStartSamp = x->writePos + x->stutterSamps;
        
            // wrap
            while( x->stutterStartSamp < 0 )
                x->stutterStartSamp += x->delaySize;
            while( x->stutterStartSamp >= x->delaySize )
                x->stutterStartSamp -= x->delaySize;

        } else
        {
            // with the transient detector turned on
            
            x->stutterStartSamp = x->lastPeakSamp;
            x->stutterEndSamp = x->stutterStartSamp - x->stutterSamps;
            // wrap
            while( x->stutterEndSamp < 0 )
                x->stutterEndSamp += x->delaySize;
            while( x->stutterEndSamp >= x->delaySize )
                x->stutterEndSamp -= x->delaySize;
        }
        
        // set read position
        x->readPos = x->stutterStartSamp;
        x->stutterSampsRead = 0;
        
        // set stutterOrBreak variable
        x->stutterOrBreak = STUTTER;
        
        post("stutter/mute on");
        
    } else
    {
        x->stutterOn = 0;
        post("stutter/mute off");
    }
}

// adjust stutter length in ms - can be any float less than the delay line
void stutterstutter_stutterlength(t_stutterstutter *x, t_floatarg f)
{
    t_float lastStutterTimeTarget = x->stutterTimeTarget;
    if(f < x->delaySize)
    {
        x->stutterTimeTarget = f;
    } else
    {
        x->stutterTimeTarget = x->delaySize - 1;
    }
    
}


// adjust stutter break length in ms - can be any float, will force it to be positive
void stutterstutter_stutterblength(t_stutterstutter *x, t_floatarg f)
{
    // copy in the break time
    x->stutterbTimeTarget = fabs(f);
}


// called when user adjusts wet/dry parameter
// 0 means that it will be completely original file
// 100 means that it will be completely the stuttered signal
void stutterstutter_wetdry(t_stutterstutter *x, t_floatarg f)
{
    // check bounds
    if( f < 0.0f )
        f = 1.0f;
    if( f > 100.0f )
        f = 0.0f;
    
    // subtract from 1 so we don't have to do this in the perform function
    x->wetdryTarget = 1.0f - (f/100.f);
}

// called when user adjusts random parameter
// 0: stuttering will never happen
// 100: means that stuttering will always happen
void stutterstutter_randomness(t_stutterstutter *x, t_floatarg f)
{
    // check bounds
    if( f < 0.0f )
        f = 0.0f;
    if( f > 100.0f )
        f = 1.0f;
    
    // when some value is less then this randomness value
    // the stutter will happen
    x->randomnessTarget = f/100.f;
}


// called when user adjusts amplitude parameter
// ranges from -60dB to 0dB and it is mapped here from 0.0 to 1.0
// note: this has been replaced with an amplitude LFO, but I've kept this here
// in case I want to put this pack into the object
void stutterstutter_amplitude(t_stutterstutter *x, t_floatarg f)
{
    // check bounds
    if( f <= -60.0f)
        x->ampTarget = 0.0f;
    if( f > 0.0f )
        x->ampTarget = 1.0f;
    
    // convert from dB to linear
    if( f > -60.0f && f <= 0.0f )
        x->ampTarget = powf(10.0f, f/20.0f);
    
}

// function to print to the console window when an incorrect 'set' message has been given
void messageUsage(void)
{
    error("please enter a message with the form 'set mute', 'set stutter', 'set transient on', 'set transient off', 'set env on', 'set env off', 'set env 100', 'set amplfo on', 'set amplfo off''.");
}

// inlet with a set message
void stutterstutter_set(t_stutterstutter *x, t_symbol *selector, int argc, t_atom *argv)
{
    const char *mute_text = "mute";
    const char *stutter_text = "stutter";
    const char *tracker_text = "tracker";
    const char *env_text = "env";
    const char *amplfo_text = "amplfo";
    const char *off = "off";
    const char *on = "on";
    
    
    if( argc == 1 )
    {
        // change stutter/mute settings
        
        if(argv[0].a_type != A_SYMBOL || (!strcmp(argv[0].a_w.w_symbol->s_name, mute_text) && !strcmp(argv[0].a_w.w_symbol->s_name, stutter_text)))
        {
            messageUsage();
            return;
        }
        
        if(strcmp(argv[0].a_w.w_symbol->s_name, mute_text) == 0)
        {
            post("setting changed to: mute chunks");
            x->muteOrStutter = SETTING_MUTE;
        } else if(strcmp(argv[0].a_w.w_symbol->s_name, stutter_text) == 0)
        {
            post("setting changed to: stutter chunks");
            x->muteOrStutter = SETTING_STUTTER;
        }
    } else if( argc == 2 )
    {
        // turn transient detector on or off
        
        if(argv[0].a_type == A_SYMBOL && argv[1].a_type == A_SYMBOL && strcmp(argv[0].a_w.w_symbol->s_name, tracker_text) == 0)
        {
            if(strcmp(argv[1].a_w.w_symbol->s_name, off) == 0)
            {
                // set the transient detector off
                post("transient detector is off");
                x->levelTrackerState = TRACKER_OFF;
            } else if(strcmp(argv[1].a_w.w_symbol->s_name, on) == 0)
            {
                // set the transient detector on
                post("transient detector is on");
                x->levelTrackerState = TRACKER_ON;
            }
            else
            {
                messageUsage( );
            }
        } else if(argv[0].a_type == A_SYMBOL && argv[1].a_type == A_SYMBOL && strcmp(argv[0].a_w.w_symbol->s_name, env_text) == 0)
        {
            // turn envelope on or off
            
            if(strcmp(argv[1].a_w.w_symbol->s_name, off) == 0)
            {
                // set the envlope off
                post("envelope is off");
                x->envState = ENV_OFF;
            } else if(strcmp(argv[1].a_w.w_symbol->s_name, on) == 0)
            {
                // set the envelope on
                post("envelope is on");
                x->envState = ENV_ON;
            } else
            {
                messageUsage( );
            }
        } else if(argv[0].a_type == A_SYMBOL && argv[1].a_type == A_FLOAT && strcmp(argv[0].a_w.w_symbol->s_name, env_text) == 0)
        {
            // modify envelope ramp length
            
            x->envSampsTarget = ( int )(argv[1].a_w.w_float/1000.0f * x->sr);
            if( x->envSampsTarget > floor(( float )x->stutterSamps/2.0f) )
            {
                post("envelope attack/decay length is greater than half the stutter length. envelope length set to half the stutter length.");
                x->envSampsTarget = ( int )floor(x->stutterSamps*0.5f);
            } else
            {
                post("envelope length changed to %f ms", argv[1].a_w.w_float);
            }
        } else if(argv[0].a_type == A_SYMBOL && argv[1].a_type == A_SYMBOL && strcmp(argv[0].a_w.w_symbol->s_name, amplfo_text) == 0)
        {
            if(strcmp(argv[1].a_w.w_symbol->s_name, off) == 0)
            {
                // set the amplitude lfo off
                post("amplitude lfo control is off - modify amplitude with control rate inlet");
                x->ampState = AMP_LFO_OFF;
            } else if(strcmp(argv[1].a_w.w_symbol->s_name, on) == 0)
            {
                // set the transient detector on
                post("amplitude lfo control is on");
                x->ampState = AMP_LFO_ON;
            }
            else
            {
                messageUsage( );
            }
        }
    }
    
    
}


// clean up memory
static void stutterstutter_free(t_stutterstutter *x)
{
	free(x->delayBuffer);
    free(x->env);
}

// new function that is called everytime we create a new stutterstutter object in pd
static void *stutterstutter_new(void)
{
    // create object from class
    t_stutterstutter *x = (t_stutterstutter *)pd_new(stutterstutter_class);
    
    // === inlets and outlets ===

    inlet_new(&x->x_obj, &x->x_obj.ob_pd, gensym("float"), gensym("stutterlength"));    // stutter length (control rate)
    inlet_new(&x->x_obj, &x->x_obj.ob_pd, gensym("float"), gensym("stutterblength"));   // stutter break length (control rate)
    inlet_new(&x->x_obj, &x->x_obj.ob_pd, gensym("float"), gensym("wetdry"));           // dry/wet parameter (control rate)
    inlet_new(&x->x_obj, &x->x_obj.ob_pd, gensym("float"), gensym("randomness"));       // randomness parameter (control rate)
    inlet_new(&x->x_obj, &x->x_obj.ob_pd, gensym("float"), gensym("amplitude"));        // amplitude control of stutters (control rate)
    inlet_new(&x->x_obj, &x->x_obj.ob_pd, gensym("set"), gensym("set"));                // set message
    
    inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);                        // amplitude with LFO (audio rate)
    
    outlet_new(&x->x_obj, gensym("signal"));                                            // output signal
    x->bangOutlet = outlet_new(&x->x_obj, gensym("bang"));                              // level detector bang
    // ===========================
    
    // sample rate (should be modified in dsp function in case it is different)
    x->sr = 44100.0f;
    
    // === user parameters ===
    // (internal numbers/mapping)
    x->wetdryTarget = 1.0f;
    x->wetdryCurrent = 1.0f;
    x->randomnessTarget = 1.0f;
    x->randomnessCurrent = 1.0f;
    x->randomNoStutter = 0;         // individual stutter on/off flag
    x->ampTarget = 1.0;
    x->ampCurrent = 1.0;
    x->ampState = AMP_LFO_OFF;
    x->stutterOn = 0;               // stutters on from bang
    x->stutterTimeTarget = 500.0f;  // stutter time
    x->stutterTimeCurrent = 500.0f;
    x->stutterbTimeTarget = 0.0f;   // stutter break time
    x->stutterbTimeCurrent = 0.0f;
    x->stutterbSamps = ( int )x->stutterbTimeCurrent / 1000.0f * x->sr;    // stutter break time in samples
    x->stutterbCnt = 0;             // sample counter for stutter breaks
    
    x->muteOrStutter = SETTING_STUTTER;
    // =======================
    
    
    // === delay line stuff ===
    x->delayTime = 0.0f;
    x->writePos = 0;
    x->delaySize = MAX_DELAY_SIZE;
    x->delayBuffer = (t_float *)malloc(sizeof(t_float) * x->delaySize);
    // zero out delay buffer
    long i;
    for(i = 0; i < x->delaySize; i++)
        *(x->delayBuffer+i) = 0.0f;
    // ===========================================
    
    
    // === level tracker stuff ===
    x->levelEstimate = 0.0f;    // for transient detection
    x->thresholdU = 0.80f;      // default thresholds for level tracker
    x->thresholdL = 0.2f;
    x->lastPeakSamp = 0;        // keep track of last transient
    x->peakDetectorState = WAIT_FOR_PEAK;
    x->levelTrackerState = TRACKER_OFF;   // level tracker off by default
    x->detectedPeak = 0;
    // ======================================
    
    // === envelope stuff ===
    x->envState = ENV_OFF;      // off by default
    x->envASDstate = ENV_S;
    x->envSampsTarget = 0;
    x->envSampsCurrent = 0;
    x->env = (t_float *)malloc(MAX_ENV_SIZE * sizeof(t_float));
    memset(x->env, 0.0f, MAX_ENV_SIZE * sizeof(t_float));    // zero out envelope
    // ======================
    
    
    // seed random number generator - add xy position so multiple objects don't all
    // seed the same when opening the patch
    srand(time(NULL) + x->x_obj.te_xpix + x->x_obj.te_ypix);
    
    return (x);
}

// called when Pd is first loaded and tells Pd how to load the class
void stutterstutter_tilde_setup(void)
{
    stutterstutter_class = class_new(gensym("stutterstutter~"), (t_newmethod)stutterstutter_new, (t_method)stutterstutter_free, sizeof(t_stutterstutter), 0, 0);
    
    // magic to declare the leftmost inlet the main inlet that will take a signal
    // installs delayTime as the leftmost inlet float
    CLASS_MAINSIGNALIN(stutterstutter_class, t_stutterstutter, delayTime);//sr);
    
    // method that will be called when dsp is turned on
    class_addmethod(stutterstutter_class, (t_method)stutterstutter_dsp, gensym("dsp"), (t_atomtype)0);
    
    // bang callback (to start and stop the stutter)
    class_addbang(stutterstutter_class, (t_method)stutterstutter_bang);
    
    // method for stutter length parameter (control rate)
    class_addmethod(stutterstutter_class, (t_method)stutterstutter_stutterlength, gensym("stutterlength"), A_FLOAT, 0);
    
    // method for stutter break length parameter (control rate)
    class_addmethod(stutterstutter_class, (t_method)stutterstutter_stutterblength, gensym("stutterblength"), A_FLOAT, 0);
    
    // method for dry/wet parameter (control rate)
    class_addmethod(stutterstutter_class, (t_method)stutterstutter_wetdry, gensym("wetdry"), A_FLOAT, 0);
    
    // method for randomness parameter (control rate)
    class_addmethod(stutterstutter_class, (t_method)stutterstutter_randomness, gensym("randomness"), A_FLOAT, 0);
    
    // method for amplitude parameter (control rate)
    class_addmethod(stutterstutter_class, (t_method)stutterstutter_amplitude, gensym("amplitude"), A_FLOAT, 0);
    
    // register callback method for inlet set message
    class_addmethod(stutterstutter_class, (t_method)stutterstutter_set, gensym("set"), A_GIMME, 0);
    
    
}

